import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Alert from 'react-bootstrap/lib/Alert';
import axios from 'axios';


class HiddenAlert extends Component {
	constructor(props, context) {
	    super(props, context);
	}

	render() {
		if (this.props.message == "") {
			return ("");
		} else {
			return (
				<Alert bsStyle={this.props.alertClass}>
					{this.props.message}
				</Alert>
			);
		}
	}
}

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			message: "",
			mobileNumber: "",
			numChars: 0,
			numSMS: 1,
			msgErrAlert: "",
			mobErrAlert: "",
			smsErrAlert: "",
			smsSuccessAlert: ""
		}
		this.handleMessageChange = this.handleMessageChange.bind(this);
		this.handleMobileNumberBlur = this.handleMobileNumberBlur.bind(this);
		this.handleFormSubmit = this.handleFormSubmit.bind(this);
	}

    // When message form object changes make sure it meets API criteria
	handleMessageChange(event) {
		const target = event.target;
		let numSMS = 1;
		if (target.value.length >= 161 && target.value.length <= 306) {
			numSMS = 2;
		} else if (target.value.length > 306) {
			numSMS = 3;
		}

		this.setState({
			message: target.value,
			numChars: target.value.length,
			numSMS: numSMS,
			msgErrAlert: (target.value.length <= 459 ? "" : "You have exceeded the message character limit"),
			smsSuccessAlert: "",
			smsErrAlert: ""
		});
	}

    // Check that mobile number meets required conditions when form object loses focus
	handleMobileNumberBlur(event) {
		const target = event.target;

		let mobNumber = target.value;
		this.setState({
			mobileNumber: mobNumber,
			mobErrAlert: (mobNumber.match(/^[0-9]{10,11}$/) ? "" : "Mobile number must consist of only 10 or 11 digits"),
			smsSuccessAlert: "",
			smsErrAlert: ""
		});
	}

	// Handles the form submission, checks if any errors are present before sub
	handleFormSubmit(event) {
		var self = this;
		if (self.state.msgErrAlert == "" && self.state.mobErrAlert == "") {
			sendSms(self.state.mobileNumber, self.state.message).then(function(response) {
				if (response.data.error.code == "SUCCESS") {
					self.setState({
						smsErrAlert: "",
						smsSuccessAlert: response.data.message
					})
				} else {
					self.setState({
						smsErrAlert: response.data.error.description,
						smsSuccessAlert: ""
					})
				}
			}).catch(function(err){
				if (err.response != undefined && err.response.status == 400) {
					self.setState({
						smsErrAlert: err.response.data.error.description,
						smsSuccessAlert: ""
					})
				} else {
					self.setState({
						smsErrAlert: "An unexpected error occurred",
						smsSuccessAlert: ""
					})
				}
			})
		} else {
			self.setState({
				smsErrAlert: "Ensure all fields are valid before submitting"
			})
		}

	}

	render() {
        // Set up tooltips to be displayed in form
		const mobileTooltip = (
			<Tooltip id="mobileTooltip">
				Receivers mobile number defined in international format.<br></br> i.e. For Australian receiver 61491570156
			</Tooltip>
		);

		const messageTooltip = (
			<Tooltip id="messageTooltip">
				The message you wish to send to the receiver. Messages can be up to 459 characters long, however messages longer than 160 characters will cost you an extra units of SMS. <br></br> Up to 160 chars = 1 SMS, Up to 306 chars = 2 SMS, Up to 459 chars = 3 SMS.
			</Tooltip>
		);

		return (
			<div className="container">
		        <div style={{marginTop: '50px'}} className="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		            <div className="panel panel-info" >
		                <div className="panel-heading">
		                    <div className="panel-title">Send SMS</div>
		                </div>

		                <div style={{paddingTop: '30px'}} className="panel-body" >

							<HiddenAlert name="msgErrAlert" message={this.state.msgErrAlert} alertClass="danger"></HiddenAlert>
							<HiddenAlert name="mobErrAlert" message={this.state.mobErrAlert} alertClass="danger"></HiddenAlert>
							<HiddenAlert name="smsErrAlert" message={this.state.smsErrAlert} alertClass="danger"></HiddenAlert>
							<HiddenAlert name="smsSuccessAlert" message={this.state.smsSuccessAlert} alertClass="success"></HiddenAlert>

	                        <form id="smsForm" className="form-horizontal" role="form">

								<div className="form-group col-sm-12">
									<label htmlFor="mobile-label">
										Mobile Number &nbsp;
										<OverlayTrigger placement="top" overlay={mobileTooltip}>
										    <i className="fa fa-info-circle" aria-hidden="true"></i>
									    </OverlayTrigger>
									</label>
		                            <div id="mobile-label" style={{marginBottom: '25px'}} className="input-group col-xs-7">
		                                <span className="input-group-addon"><i className="glyphicon glyphicon-phone"></i></span>
		                                <input id="mobile-number" type="text" onBlur={this.handleMobileNumberBlur} className="form-control" name="mobileNumber" placeholder="Receivers Mobile Number"></input>
		                            </div>
								</div>

								<div className="form-group col-xs-12">
									<label htmlFor="message-label">
										Message &nbsp;
										<OverlayTrigger placement="top" overlay={messageTooltip}>
											<i className="fa fa-info-circle" aria-hidden="true"></i>
										</OverlayTrigger>
									</label>
									<p></p>
		                            <div style={{marginBottom: '25px'}} className="input-group">
		                                <textarea onChange={this.handleMessageChange} style={{minWidth: '100%'}} className="form-control" rows="7" cols="100" name="message" placeholder="Message to send"></textarea>
		                            </div>

									<div className="pull-right">
										<p id="num-chars"> {this.state.numChars} / 459 characters ({this.state.numSMS} SMS)</p>
									</div>
								</div>

								<div className="col-sm-12 ">
		                            <div style={{marginTop: '10px'}} className="form-group">
		                                <div className="col-sm-12 controls">
		                                  	<a id="btn-send-sms" href="#" className="btn btn-success pull-right" onClick={this.handleFormSubmit}>Send SMS </a>
		                                </div>
		                            </div>
								</div>

	                    	</form>
		                </div>
			        </div>
				</div>
			</div>
		)
	}
}

function sendSms (to, message) {
	return axios({
		method: 'post',
		url: '/sendSms',
		data: {
			to: to,
			smsMessage: message,
		}
	})
}


export default App;
