process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');

let app = require('../app');
let should = chai.should();

chai.use(chaiHttp);

/*
 * Test the /POST sendSms route
 */
describe('/POST sendSms', () => {
    it('it should return a 400 FIELD_EMPTY without mobile number', (done) => {
        chai.request(app)
            .post('/sendSms')
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.property('code');
				res.body.error.should.have.property('code').eql('FIELD_EMPTY');
				res.body.error.should.have.property('description');
				res.body.error.should.have.property('description').eql('A mobile number is required to send a SMS');
                done();
            });
    });
});

describe('/POST sendSms', () => {
    it('it should return a 400 INVALID_FIELD with a mobile number containing non-digit characters', (done) => {
        let sms = {
			to: "61444a12345",
            smsMessage: "This message should fail"
        }
        chai.request(app)
            .post('/sendSms')
			.send(sms)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.property('code');
				res.body.error.should.have.property('code').eql('INVALID_FIELD');
				res.body.error.should.have.property('description');
				res.body.error.should.have.property('description').eql('Mobile number must consist of only 10 or 11 digits');
                done();
            });
    });
});

describe('/POST sendSms', () => {
    it('it should return a 400 FIELD_EMPTY without a message', (done) => {
		// Fake number from ACMA
		let sms = {
			to: "61491570156",
            smsMessage: ""
        }
        chai.request(app)
            .post('/sendSms')
			.send(sms)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.property('code');
				res.body.error.should.have.property('code').eql('FIELD_EMPTY');
				res.body.error.should.have.property('description');
				res.body.error.should.have.property('description').eql('A message is required to send a SMS');
                done();
            });
    });
});

describe('/POST sendSms', () => {
    it('it should return a 400 INVALID_FIELD with a message longer than 459 characters', (done) => {
		// Fake number from ACMA
		let sms = {
			to: "61491570156",
            smsMessage: "a".repeat(460)
        }
        chai.request(app)
            .post('/sendSms')
			.send(sms)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.property('code');
				res.body.error.should.have.property('code').eql('INVALID_FIELD');
				res.body.error.should.have.property('description');
				res.body.error.should.have.property('description').eql('You have exceeded the message character limit');
                done();
            });
    });
});

describe('/POST sendSms', () => {
    it('it should return a 200 success with all form fields fitting criteria', (done) => {
		// Fake number from ACMA
		let sms = {
			to: "61491570156",
            smsMessage: "This sms should work"
        }
        chai.request(app)
            .post('/sendSms')
			.send(sms)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
				res.body.should.have.property('message').eql('SMS was successfully sent to recipient');
                res.body.error.should.have.property('code');
				res.body.error.should.have.property('code').eql('SUCCESS');
				res.body.error.should.have.property('description');
				res.body.error.should.have.property('description').eql('OK');
                done();
            });
    });
});
