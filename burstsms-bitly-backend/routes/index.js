var express = require('express');
var router = express.Router();
var axios = require('axios');
var base64 = require('base-64');
require('dotenv').config()

getBitlyToken();

/* POST send sms */
router.post('/sendSms', function(req, res) {
	// Revalidate form inputs depending on criteria
	if (req.body.to === null || req.body.to === undefined) {
		res.status(400).json({
			error: {
				code: "FIELD_EMPTY",
				description: "A mobile number is required to send a SMS"
			}
		})
	} else if (!req.body.to.match(/^[0-9]{10,11}$/)) {
		res.status(400).json({
			error: {
				code: "INVALID_FIELD",
				description: "Mobile number must consist of only 10 or 11 digits"
			}
		})
	} else if (req.body.smsMessage === null || req.body.smsMessage === undefined || !req.body.smsMessage.trim().length) {
		res.status(400).json({
			error: {
				code: "FIELD_EMPTY",
				description: "A message is required to send a SMS"
			}
		})
	} else if (req.body.smsMessage.length > 459) {
		res.status(400).json({
			error: {
				code: "INVALID_FIELD",
				description: "You have exceeded the message character limit"
			}
		})
	} else {
		var messageArr = req.body.smsMessage.split(' ');
		var newMessage = "";
		var shortLinkPromises = [];
		for (var i = 0; i < messageArr.length; i++) {
            // If word is URL place shortlink promise in array at same position as the word, otherwise put null
			if (isURL(messageArr[i])) {
				if (!messageArr[i].match('^http')) {
					messageArr[i] = "http://" + messageArr[i]
				}

				shortLinkPromises[i] = axios.get('https://api-ssl.bitly.com/v3/shorten?access_token='+bitlyToken+'&longUrl='+encodeURIComponent(messageArr[i]));
			} else {
				shortLinkPromises[i] = null
			}
		}

		Promise.all(shortLinkPromises).then(function(shortLinkResults) {
			// Replace URLs with bitly shortlinks
			for (var i = 0; i < shortLinkResults.length; i++) {
				if (shortLinkResults[i] != null) {
					if (shortLinkResults[i].data.status_code == '200') {
						messageArr[i] = shortLinkResults[i].data.data.url.replace(/^(http|https):\/\//,"")
					}
				}
			}
			newMessage = messageArr.join(' ')

            // Send sms to recipients
			sendSms(req.body.to, newMessage).then(function(smsResponse) {
				res.status(200).json({
					message: "SMS was successfully sent to recipient",
					error: {
						code: "SUCCESS",
						description: "OK"
					}
				})
			}).catch(function(e) {
				res.status(400).json({
					error: {
						code: e.response.data.error.code,
						description: e.response.data.error.description
					}
				})
			})
		})
	}
});

// BurstSms send-sms api wrapper
function sendSms(to, message) {
	return axios({
		method: 'get',
		url: 'https://api.transmitsms.com/send-sms.json',
		headers: {'Authorization': "Basic " + base64.encode(process.env.BURST_KEY + ":" + process.env.BURST_SECRET)},
		params: {
			to: to,
			message: message
		}
	})
}

// Check if a str is a url
function isURL(str) {
	var pattern = new RegExp('(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})','i'); // fragment locator
	return pattern.test(str);
}


// Get bitly OAuth2 token
function getBitlyToken() {
	console.log('gettingToken')
	axios({
		method: 'post',
		url: 'https://api-ssl.bitly.com/oauth/access_token',
		headers: {
			'Authorization': "Basic " + base64.encode(process.env.BITLY_USERNAME + ":" + process.env.BITLY_PASSWORD),
			'grant_type': 'password'
		},
	}).then(function(result) {
		bitlyToken = result.data
	})
}

module.exports = router;
