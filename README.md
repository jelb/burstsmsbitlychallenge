# BurstSMS Bit.ly Challenge README

Project was created and tested on NodeJS v6.11.0

To initially set up and start backend environment:  
    1. Navigate to /burstsms-bitly-backend  
    2. Create ".env" in the root directory of /burstsms-bitly-backend containing:  
        BURST_KEY=<YOUR BURSTSMS KEY>
        BURST_SECRET=<YOUR BURSTSMS SECRET>
        BITLY_USERNAME=<YOUR BITLY USERNAME>
        BITLY_PASSWORD=<YOUR BITLY PASSWORD>  
    2. run 'npm install' to install all required dependencies  
    3. run 'npm start' to start the server in order to receive requests from the client.  
	
To initially set up and start client environment:  
    1. Navigate to /burstsms-bitly-client  
    2. run 'npm install' to install all required dependencies  
    3. run 'npm start' to start the client.  
	
To run the backend tests:  
    1. Navigate to /burstsms-bitly-backend  
    2. run 'npm test'  